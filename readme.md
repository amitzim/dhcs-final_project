# Sentiment Analysis for Parahumans Worm

As a final project in Digital Humanities For CS/191 course
we were asked to perform sentiment analysis for a piece of literature.

The analysis is performed on an e-book called "Worm" by ***Wildbow***
and done with sentiment analysis package in R language (taught in class) by ***Matthew Jockers***.

Parahumans, Worm: 
```
https://parahumans.wordpress.com/
```
Syuzhet By Mathhew Jockers:
```
https://cran.r-project.org/web/packages/syuzhet/vignettes/syuzhet-vignette.html
```
DHCS bgu:
```
https://www.cs.bgu.ac.il/~dhcs191/Main
```

## Getting Started

### Prerequisites

To run the project you need to install:
```
* python3.6
* R 3.5.2
* 'syuzhet' package
* python libraries:
	* Beautiful soup
	* matplotlib.pyplot
	* requests
```

### Installing

Download the project:
```
git clone https://amitzim@bitbucket.org/amitzim/dhcs-final_project.git
```

### How to run it

```
cd dhcs-final_project
```
```
python3 main.py arc
```
Or
```
python3 main.py chapter
```
