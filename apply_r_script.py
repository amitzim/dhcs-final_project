import os
from IPython import embed
from bs4 import BeautifulSoup
from subprocess import call
import matplotlib.pyplot as plt
# import numpy as np

# STEP 1
path_to_files = "filestoanalize/"


def create_plot_for_each_chapter(sorted_files):
	with open("temp/sentiment_per_chapter", 'w') as f:
		pass
	for file in sorted_files:
		with open(path_to_files + file, 'r') as f:
			data = f.read()
			soup = BeautifulSoup(data, 'xml')
			data = soup.find('body').text.strip()
			with open('temp/final_project', 'w') as k:
				k.write(data)
			call(["Rscript", "script.R", "temp/sentiment_per_chapter"])

	with open('temp/sentiment_per_chapter', 'r') as f: 
		data = f.readlines()

	res = list(map(lambda x: float(x.strip()), data))

	# embed()
	# Polyfit
	x = list(map(lambda x: x.strip('.txt').split(' ')[-1], sorted_files))
	# y = normalize(res)
	# z = np.polyfit(x, y, 1)
	# f = np.poly1d(z)
	# x_new = np.linspace(x[0], x[-1], 50)
	# y_new = f(x_new)
	# plt.plot(x,y,'o', x_new, y_new)

	plt.plot(x, normalize(res), 'r')
	plt.xlabel('Chapter', fontsize=18)
	plt.xticks(rotation=90)
	plt.ylabel('Sentiment', fontsize=18)
	plt.title("Sentiment Per Chapter")

	# plt.plot(sorted_files, res, 'r')
	plt.show()


def create_plot_per_arc(sorted_files):
	arcs = dict().fromkeys(list(range(1, 32)))
	
	for arc in arcs.keys():
		arcs[arc] = []
	
	for file_name in sorted_files:
		arcs[int(file_name.split('.')[0])].append(file_name)

	with open("temp/sentiment_per_arc", 'w'):
		pass
	for arc in arcs.keys():
		files = arcs[arc]
		arcdata = ""
		for file in files:
			with open(path_to_files + file, 'r') as f:
				data = f.read()
				soup = BeautifulSoup(data, 'xml')
				data = soup.find('body').text.strip()
				arcdata += data

		with open('temp/final_project', 'w') as k:
			k.write(arcdata)

		call(["Rscript", "script.R", "temp/sentiment_per_arc"])


	with open('temp/sentiment_per_arc', 'r') as f: 
		data = f.readlines()

	res = list(map(lambda x: float(x.strip()), data))
	plt.plot(arcs.keys(), normalize(res), 'r')
	plt.xlabel('Arc', fontsize=18)
	plt.ylabel('Sentiment', fontsize=18)
	plt.title("Sentiment Per Arc")
	plt.show()


def normalize(vec):
	min_val = min(vec)
	max_val = max(vec)

	normalized = list(map(lambda x: (x - min_val) / (max_val - min_val), vec))
	return normalized

# create_plot_per_arc(sorted_files)
# create_plot_for_each_chapter(sorted_files)
