from bs4 import BeautifulSoup
import requests
from math import ceil
from re import sub
from multiprocessing.pool import ThreadPool
from multiprocessing import cpu_count
from datetime import datetime
from IPython import embed

already_parsed_urls = []


start ="""<?xml version="1.0" encoding="UTF-8"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?>
<?xml-model href="http://www.tei-c.org/release/xml/tei/custom/schema/relaxng/tei_all.rng" type="application/xml"
	schematypens="http://purl.oclc.org/dsdl/schematron"?>
<?xml-stylesheet type="text/xsl" href="custom.xsl"?>
"""


def update_bad_url(url):
	with open('crawler_log/bad_urls.txt', 'a') as f:
		f.write(url + '\n')


def find_right_article(soup):
	all_divs = soup.find_all('article', id='post-2404')
	return all_divs[0]


def get_all_links_from_article(article):
	res = set()
	links = article.find_all('a')
	for link in links:
		res.add(link['href'])

	return res

def get_title(soup):

	title = soup.find_all('h1', class_ = "entry-title")[0]
	if title.text != 'Worm':
		return title.text
	else:
		return title.find('a').text
	raise Exception("No title?!")


def get_text(soup):
	content_div = soup.find_all('div', class_ = "entry-content")[0]
	paragraphs = content_div.find_all('p')
	
	content = ""
	for p in paragraphs:
		if len(p.find_all('a')) > 0:
			continue
		
		content += create_tag("p", str(p.text), 0)
		content += '\n'

	return content
			

def get_date(soup):
	meta = soup.find_all('div', class_ = 'entry-meta')
	try:
		date = meta[0].text.strip('\\n').strip('\\t')
	except Exception:
		date = ""
	return date


def parse_arcs(url):
	if url in already_parsed_urls:
		return
	try:
		content = str(requests.get(url).content)
		soup = BeautifulSoup(content, 'lxml')
		title = get_title(soup)
		text = get_text(soup)
		text = sub(r'\\xc2\\xa0', '', text)
		text = sub(r'\\xe2\\x96\\xa0', '', text)
		text = sub(r'\\xe2\\x80\\x9c', '', text)
		text = sub(r'\\xe2\\x80\\x99d', '', text)
		text = sub(r'\\xe2\\x80\\x99t', '', text)
		text = sub(r'\\xe2\\x80\\x9d', '', text)
		date = get_date(soup)
		meta = build_metadata(title, date, url)
		with open('data/' + title + '.txt', 'w') as f:
			f.write(start)
			text_tag = create_tag("text", create_tag("body", text, 0), 0)
			f.write(create_tei_tag(meta + text_tag))
				
	except Exception:
		update_bad_url(url)
		return

	with open('crawler_log/already_parsed.txt', 'a') as f:
		f.write(url + '\n')
		already_parsed_urls.append(url)

def create_tei_tag(content):
	open_tag = "<TEI xmlns=\"http://www.tei-c.org/ns/1.0\">"
	closing_tag = "</TEI>"
	return add_tab_to_tag(open_tag, 0) + '\n' + add_tab(content, 1) + '\n' + add_tab_to_tag(closing_tag, 0) + '\n'


def build_metadata(title, date, url):
	author = "Wildbow"
	current_time = datetime.now()
	formated_time = current_time.strftime("%d.%m.%y at %H:%M:%S")
	date = date.strip('Posted on ').strip('  by  wildbow')
	author_data = create_tag("author", author, 0)
	publication_data = create_tag("publicationStmt", create_tag("publisher", "www.wordpress.com", 0), 0)
	title_stmt = create_tag("titleStmt", create_tag("title", title, 0) + author_data, 0)
	


	src_desc = "Dowloaded from: %s at %s by %s.\nThis record was published at %s" % (url, formated_time, "Yonatan Shavit and Amit Zimbalist", date)
	# src_desc += create_tag("downloadTime", formated_time, 0)
	# src_desc += create_tag("downloadedBy", "Yonatan Shavit and Amit Zimbalist", 0)
	# publish_data = create_tag("publishDate", date, 0)
	sourceDesc = create_tag("sourceDesc", create_tag("p", src_desc, 0), 0)
	file_desc_content = title_stmt + publication_data + sourceDesc
	file_desc_tag = create_tag("fileDesc", file_desc_content, 0)
	tei_header_tag = create_tag("teiHeader", file_desc_tag, 0)

	return tei_header_tag


def tei_header():
	pass

def add_tab_to_tag(tag, counter):
	if counter == 0:
		return tag
	else:
		return add_tab_to_tag('\t' + tag, counter - 1)

def add_tab(content, counter):
	content_as_lines = content.split('\n')
	result = ""
	for line in content_as_lines:
		result += add_tab_to_tag(line, counter) + '\n'
	return result


def create_tag(tag_name, content, depth):
	open_tag = "<%s>" % tag_name
	closing_tag = "</%s>" % tag_name
	return add_tab_to_tag(open_tag, depth) + '\n' + add_tab(content, depth + 1) + '\n' + add_tab_to_tag(closing_tag, depth) + '\n'


def crawl(chunk):
	list(map(lambda x: parse_arcs('https://' + x) if 'https://' not in x else parse_arcs(x), chunk))
	

def main():

	with open('crawler_log/already_parsed.txt', 'r') as f:
		already_parsed_urls = f.readlines()
	url_table_of_contents = 'https://parahumans.wordpress.com/table-of-contents/'
	content = str(requests.get(url_table_of_contents).content)
	soup = BeautifulSoup(content, 'lxml')

	art = find_right_article(soup)
	res = list(get_all_links_from_article(art))
	# x = res[0]
	# parse_arcs(x)
	workers = cpu_count()
	executor = ThreadPool(workers)
	chunk_size = ceil(len(res) / workers)
	chunks = [res[x:x + chunk_size] for x in list(range(0, len(res), chunk_size))]
	executor.map(crawl, chunks)
	executor.close()


if __name__ == '__main__':
	main()
