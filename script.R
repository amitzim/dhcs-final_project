# library('plotly')
library('syuzhet')

args <- commandArgs(TRUE)
output_fileName <- args[1]
input_fileName <- 'temp/final_project'
my_example_text <- readChar(input_fileName, file.info(input_fileName)$size)

s_v <- get_sentences(my_example_text)
s_v_sentiment <- get_sentiment(s_v)
sum_chapter <- sum(s_v_sentiment)

out <- file(output_fileName, 'a')
a <- sprintf("%0.4f", sum_chapter)
writeLines(a, out)
close(out)



# p <- plot_ly(type = 'scatter', mode = 'lines') %>%
#   add_trace(
#     x = c(1:length(s_v)), 
#     y = s_v_sentiment, 
#     text = s_v,
#     # marker = list(color='green'),
#     hoverinfo = text,
#     showlegend = F
#   )

# p
