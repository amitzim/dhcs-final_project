from os import mkdir, listdir, rename
from sys import argv
import crawler
import functools
import apply_r_script
from IPython import embed

functions = {"arc": apply_r_script.create_plot_per_arc, "chapter" : apply_r_script.create_plot_for_each_chapter}
unnecessary_file_names = ["Interlude", "Glow-", "This is somewhat embarrassing, isn’t it?"]

def setup():
	try:
		mkdir("temp")
		mkdir("data")
		mkdir("filestoanalize")
		mkdir("crawler_log")
		with open('temp/final_project', 'w') as f:
			pass
		with open('temp/sentiment_per_arc', 'w') as f:
			pass
		with open('temp/sentiment_per_chapter', 'w') as f:
			pass
		
		with open('crawler_log/already_parsed.txt', 'w') as f:
			pass
		with open('crawler_log/bad_urls.txt', 'w') as f:
			pass
			
	except FileExistsError:
		pass

def move_correct_files_to_filestoanalize():
	data_files = listdir("data")
	for file_name in data_files:
		if (unnecessary_file_names[0] in file_name) or (unnecessary_file_names[1] in file_name) or (unnecessary_file_names[2] in file_name):
			continue
		else:
			new_filename = file_name.split()[-1]
			if "e" in new_filename:
				new_filename = "31" + new_filename[1:]
			rename("data/" + file_name, "filestoanalize/" + new_filename)



def mySort(x, y):
	left = x.strip('.txt')
	right = y.strip('.txt')
	left = left.split('.')
	right = right.split('.')
	if int(left[0]) < int(right[0]):
		return -1
	if int(left[0]) > int(right[0]):
		return 1
	if int(left[0]) == int(right[0]):
		if int(left[1]) < int(right[1]):
			return -1
		return 1

def sort_files():
	path_to_files = "filestoanalize/"
	file_names = listdir(path_to_files)
	# lst = list(map(lambda x: float(x.strip('.txt')), file_names))

	sorted_files = sorted(file_names, key=functools.cmp_to_key(mySort))
	return sorted_files


def generate_plot(sorted_files, graph_type):
	try:
		functions[graph_type](sorted_files)
	except KeyError:
		print("Wrong graph type: %s" % graph_type)
		print("graph_type should be: %s" % str(list(functions.keys())))

if __name__ == '__main__':
	setup()
	if listdir("data") == []:
		crawler.main()

	move_correct_files_to_filestoanalize()
	sorted_files = sort_files()
	try:
		graph_type = argv[1]
	except KeyError:
		print("graph_type not specified")
		exit(0)

	generate_plot(sorted_files, graph_type)